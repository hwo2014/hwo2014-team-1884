import track
import history
import common
import ai
import math
import time
import random

class SwitcherAI(ai.AI):
    """ Choose a lane that has a shortest distance between switches
    
    class variables (= self.xxx):
    switching    - Bool indicating if switch has been scheduled
    turbo_boost  - If not enough (or too much) slip, then modify throttle by 
                   this amount [currently, not used]
    target_speed - Target speed to maintain.
    next_lane    - This lane has been evaluated as the best for the next part.
    last_pindex  - Index of the previous piece that our car was on. 
                   Used for detecting piece change.
    swdist       - List of lists. Contains results of path evaluation between 
                   two switching points. One value per each lane. 
                   swdist[4][0] indexes the first lane (0) of the fifth switch 
                   piece, not track piece #5!. The first index indexes
                   switching piece, the second index the lane.
    trackinfo    - List of lists. Contains the 'optimal' time to traverse
                   a given piece using given lane.
                   Optimality is based on maintaining speed get_max_speed()
                   throughout the whole piece, and not braking for the next
                   tile. trackinfo[3][0] gives the time for third piece, on 
                   lane 0.
    prev_slip    - Previous tick's slip angle. Used for delta.
    accel_chart  - Acceleration chart for throttle = 1.0. Indexed by
                   current speed (integer part only). Estimation only.
    """
    def __init__(self, track, history, control):
        """ Some internal variables, not visible beyond the constructor.
        These are used for one purpose throughout the constructor.
        Variable 'tmp' in contrast is used for many things, therefore it is
        not documented.
        
        tracklen    - Length of the track in pieces.
        sws         - List of indexes to track pieces which contain a switch.
        lanes       - Number of lanes.
        
        """
        super(SwitcherAI, self).__init__(track, history, control)
        random.seed(time.time())
        print "Switching AI stared!"

        self.prev_slip = 0.0
        self.switching = False                # switch scheduled
        self.turbo_boost = 0.0                # not enough slip --> turbooo
        self.target_speed = 10.0              # Try to achieve this peed.
        self.next_lane = 0                    # switch to this lane asap
        self.last_pindex = -1                 # Last piece index
        self.turbo_at = -1
        self.secret = 0.25

        self.accel_chart = 20 * [0.01]
        self.decel_chart = 20 * [0.2]

        tracklen = len(self.track.pieces)     # Length of track in pieces
        sws = self.track.get_switches()       # all switches
        lanes = self.track.get_lanes().amount() # # of lanes

        tmp = len(self.track.switches) * lanes
        self.swdist = tmp * [0.0]
        

        for i in xrange(0,9):
            self.accel_chart[i] = 0.20 - 0.02*i
            self.decel_chart[i] = 0.02*i

        
        self.trackinfo = (tracklen*lanes) * [0]
        # For each part of a track separated by a switch, calculate the
        # optimum time, assuming max. speed estimation by get_max_speed()
        #
        # As a side effect, look for the longest straight as well (for turbo).

        for i in xrange(0,len(self.track.pieces)):
            tmp = []
            for z in xrange(0, lanes):
                dist = self.track.pieces[i].get_len(z)
                maxspeed = self.get_max_speed(self.track.pieces[i].get_anglen(z))
                tmp.append(dist/maxspeed)
            self.trackinfo[i] = tmp

        self.longest = [(-1,0),(-1,0)]
        startstr = -1
        overflow = 0.0
        for i in xrange(0, len(self.track.pieces)):
            if self.track.pieces[i].is_straight():
                if startstr == -1:
                    startstr = i
                else:
                    if i == len(self.track.pieces)-1:
                        if overflow > 0.0:
                            newlen = self.track.get_dist_btw_pieces(startstr, i, 0)
                            newlen += overflow
                            self.add_new_max(startstr, newlen)
            else:
                if startstr != -1:
                    newlen = self.track.get_dist_btw_pieces(startstr, i, 0)
                    self.add_new_max(startstr, newlen)
                    if startstr == 0:
                        overflow = newlen
                    startstr = -1

        print "AI: Longest straights are at pieces: %d and %d with lengths at %fm and %fm" % (self.longest[0][0], self.longest[1][0], self.longest[0][1], self.longest[1][1])


        self.once = False
# two different strategies
# distance_metric - choose shortest path
# time_metric - choose fastest path
        for i in xrange(0,len(sws)):
            j = (i+1) % len(sws)
            pi = sws[i]
            pj = sws[j]
#            print "Path: %d" % (i,),
            for z in xrange(0, lanes):
#                metric = self.distance_metric(pi, pj, z)
                metric = self.time_metric(pi, pj, z)
                a1 = self.distance_metric(pi, pj, z)
                a2 = self.time_metric(pi, pj, z)
#                print "Lane %d: {%f, %f}, " % (z, a1, a2),

                self.swdist[i*lanes+z] = metric
            print ""

    def distance_metric(self, startIdx, endIdx, lane):
        """ Provide distance between %startIdx% and %endIdx% on %lane%.
        """
        return self.track.get_dist_btw_pieces(startIdx, endIdx, lane)

    def time_metric(self, startIdx, endIdx, lane):
        """ Provide optimal time between %startIdx% and %endIdx% on %lane%.
        %endIdx% is excluded.
        """
        t = 0
        if endIdx < startIdx:
            return self.time_metric(startIdx, len(self.track.pieces), lane) + \
                   self.time_metric(0, endIdx, lane)

        for i in xrange(startIdx, endIdx):
            t += self.trackinfo[i][lane]
        return t


    def add_new_max(self, index, value):
        """ Get the worst (smallest) value from dists dictionary
        """
        # update if index 0 is in the longest list
        if self.longest[0][0] == 0:
            self.longest[0] = (index, value)
            return
        elif self.longest[1][0] == 0:
            newval = self.longest[1][1] + value
            self.longest[self.longest[0], (-1, 0)]
            self.add_new_max(index, newval) 
            # need to re-add because it may be bigger than the longest[0]
            return

        if self.longest[0][1] < value:
            self.longest = [(index, value), self.longest[0]]
        elif self.longest[1][1] < value:
            self.longest = [self.longest[0], (index, value)]

    def dist_to_target_speed(self, speed_target, speed_now, braking=True):
        change = speed_target - speed_now
        val = int(speed_now)

        if val > 10.0:
            decel_val = self.decel_chart[10]
            accel_val = self.accel_chart[10]
        elif val < 0.0:
            decel_val = self.decel_chart[0]
            accel_val = self.accel_chart[0]
        else:
            decel_val = self.decel_chart[val]
            accel_val = self.accel_chart[val]

        if braking:
            if speed_now > 18.0:
                return 1000.0    # PANIC mode
            if abs(change) <= decel_val:
                return 15.0
            else:
                nspeed = speed_now - decel_val

        else:
            if abs(change) <= accel_val:
                return 0
            else:
                nspeed = speed_now + accel_val

#        print "change = %f, braking = %s, speed= %f, decel = %f, accel=%f, val=%d" % (change, str(braking), speed_now, decel_val, accel_val, val)
        return nspeed + self.dist_to_target_speed(speed_target, nspeed, braking)


    def change_at_next_switch(self, now):

        # distance to the next switch
        dist = now.nswitch_p
        pidx = now.inst.pi + dist + 1
        if pidx > len(self.track.pieces):
            pidx -= len(self.track.pieces)

        sws = self.track.get_switches()
        sidx = sws.index(pidx)

        # so: sidx = index to the current switch in the switch table
        # Let's choose the shortest one:
        mini = 900000
        chosen = 900
        i_am_best = False
        blah = self.track.get_lanes().amount()
        for z in xrange(0,blah):
#            print "Lane: %d is %f long" % (z, self.swdist[i*blah+z])
            item = self.swdist[sidx*blah+z]
            if item == mini:
                # we have a tie!
                # Am I already on tieing lane?
                if now.inst.lane == chosen or now.inst.lane == z:
                    # yes, I am!
                    i_am_best = True
                chosen = z
            elif item < mini:
                if now.inst.lane == z:
                    i_am_best =  True
                else:
                    i_am_best = False
                mini = item
                chosen = z

        if i_am_best:
            return -1

        if now.inst.lane < chosen:
            self.switch_lane("Right")
        else:
            self.switch_lane("Left")
        self.next_lane=chosen
        return 0

    def get_max_speed(self, anglen):
        # hatusta
        if anglen < 0.3:
            return 10.0
        elif anglen < 0.4:
            return 9.75-self.secret
        elif anglen < 0.5:
            return 8.25-self.secret
        elif anglen < 0.6:
            return 7.45-self.secret
        elif anglen > 0.600001:
            red = (anglen - 0.6)*10*self.secret
            return 6.5 - red
        else:
            return 5.25-self.secret


    def run_tick(self, tick, now):
        """ Where's the logic?
        
        Code is divided into N blocks, each of which does the following:
        Block 1 - Check if we have changed to next track piece. I'm not sure
                  if this will be needed.

        Block 2 - Schedule lane switch, if appropriate.

        Block 3 - If we are about to switch lanes, we should take it into
                  consideration when calculating target speed.

        Block 4 - Determine target_speed based on current and the next piece.

        Block 5 - Slippage adjustments: traction control

        Block 6 - Final calculations on how to achieve target_speed
                  the fastest.
        """


        # BLOCK 1 - change of track piece
        if self.last_pindex != now.inst.pi:
            self.last_pindex = now.inst.pi
            new_piece = True
        else:
            new_piece = False

        # Now that we can estimate breaking distances, try to use turbo.
#        if self.turbo_at > -1:
#            if now.inst.pi == self.turbo_at:
#                self.use_turbo()


        # BLOCK 2 - schedule switch
        if not now.on_switch():
            if not self.switching:
                if self.change_at_next_switch(now) == 0:
#                    print "Switching lanes"
                    self.switching = True
        else:
            if self.switching == True:
#                print "AI: Lane switched"
                self.switching = False


        # What's the target speed for the next curve?
        pidx = (now.inst.pi + now.ncurve_p + 1) % len(self.track.pieces)
        piece = self.track.pieces[pidx]
        speed_nc = self.get_max_speed(piece.get_anglen(now.inst.lane))

        # Are we already under that target?
        apply_brakes = False
        print "Next curve in: %fm, max speed=%f" % (now.ncurve_d[now.inst.lane], speed_nc)
        if self.history.get_current_speed() > speed_nc:
            # what's our braking distance to that place?
            bdist = self.dist_to_target_speed(speed_nc, self.history.get_current_speed(), True)
            
            # Should we start braking?
            if bdist > now.ncurve_d[now.inst.lane]:
                print "AI: Hit da brakes. Curve in: %fm (est. braking dist: %fm)" % (now.ncurve_d[now.inst.lane], bdist)
                apply_brakes = True
            else:
                apply_brakes = False

        # BLOCK 3 - switching soon (this or the next piece)
        [np] = self.track.get_next_pieces(now.inst, 1)
        speed = self.history.get_current_speed()
        crt_al = now.piece.get_anglen(now.inst.lane)

        if self.switching == True:
            if np.index in self.track.get_switches():                
                nxt_al = np.get_anglen(self.next_lane)
            else:
                nxt_al = np.get_anglen(now.inst.lane)
        else:
            nxt_al = np.get_anglen(now.inst.lane)





        # BLOCK 4 - target_speeds
        if now.piece.is_straight():
            self.target_speed = 15.0
#            if np.is_straight():
#                print "AI: S -> S"
#                self.target_speed = 10.0
#            else:
#                print "AI: S -> C"
#                self.target_speed = self.get_max_speed(nxt_al)
        else:
            if np.is_straight():
#                print "AI: C -> S"
#                if math.fabs(now.inst.angle) < 40.0:
#                    # start adding some speed
#                    self.target_speed += 0.3
                    pass
            else:
#                # We are on a long bender[sic]. Correct target speed if 
#                # necessary.
                if nxt_al > crt_al:
#                    # The curve tightens. Start slowing down!
#                    print "AI: C (%f) -> C+ (%f)" % (crt_al, nxt_al)
                    self.target_speed = self.get_max_speed(nxt_al)
                else:
#                    # The curve opens up (or continues). Pedal to the metal!
#                    print "AI: C (%f) -> C- (%f)" % (crt_al, nxt_al)
#                    if now.ipd_percent > 0.5:
#                        self.target_speed = self.get_max_speed(nxt_al)
#                    else:
                    self.target_speed = self.get_max_speed(crt_al)

        # BLOCK 5 - Throttle control

        diff = self.target_speed - speed
        
        # If we had nice acceleration/deceleration charts we could use 
        # them instead of my very simple approach.

        if diff < 0:
            if diff < -0.15:   # magic value... deceleration 
            # we are too fast
                thr = 0.0
            else:
                # Due to slow throttle to power lag, let's start throttling up
                # half a target_speed could be ok.
                thr = self.target_speed / (2*10)
        else:
            acc = self.accel_chart[(int(speed) % 11)]
            # we need to gain "diff" speed, and our acceleration is "acc"
            # at throttle 1.0. Can we achieve target_speed in one tick?
            if speed + acc < self.target_speed:
                thr = 1.0
            else:
                # yes we can, so let's not over do it.
                # diff = [0...acc] (other values already handled)
                thr = self.target_speed/10 + diff / acc
                # linear extrapolation: if diff = 0.05, and acc = 0.10 
                # (at throttle 1.0), then surely we will gain 0.05 in half a
                # throttle --> 0.05 / 0.1 = 0.5.
                # This may lead to thr > 1.0, but that will be sorted out
                # later.





        # BLOCK 6 - traction control
        #
        # If slip > 55.0 --> power out
        # Else do magic.
        
        if abs(now.inst.angle) > 55.0:
            thr = 0.0
        else:
            diff = now.inst.angle - self.prev_slip
            if now.inst.angle > 0:
                if diff > 3.5:
                    # Huge slippage. 
                    thr = 0.0
                elif diff > 2:
                    # Serious slippage... but, let's look at the current angle.
                    # maybe we don't need to do anything yet.
                    if abs(now.inst.angle) < 10.0:
                        pass
                    elif abs(now.inst.angle) < 25.0:
                        thr /= 2.0
                    else:
                        thr = 0.0
                elif diff > 1:
                    # Minor slippage
                    pass
                elif diff < -1:
                    if abs(now.inst.angle) < 40.0:
                        thr += 0.2
            else:
                if diff < -3.5:
                    # Huge slippage. 
                    thr = 0.0
                elif diff < -2:
                    # Serious slippage... but, let's look at the current angle.
                    # maybe we don't need to do anything yet.
                    if abs(now.inst.angle) < 10.0:
                        pass
                    elif abs(now.inst.angle) < 25.0:
                        thr /= 2.0
                    else:
                        thr = 0.0
                elif diff < -1:
                    # Minor slippage
                    pass
                elif diff > 1:
                    if abs(now.inst.angle) < 40.0:
                        thr += 0.2



        if apply_brakes == True:
            print "AI: Braking"
            thr = 0.0

# FAILSAFE: Just in case we get ridiculous values, let's not stop on the
# track.
        if speed < 3.0 and self.target_speed == 0.0:
            thr = 0.3

        if thr < 0.0:
            thr = 0.0
        elif thr > 1.0:
            thr = 1.0

        if thr == 1.0:
            if now.inst.pi == self.longest[0][0] and self.longest[0][1] > 150.0:
                if self.use_turbo():
                    print "TURBO LONG"
            elif now.inst.pi == self.longest[1][0] and self.longest[1][1] > 150.0:
                if self.use_turbo():
                    print "TURBO SHORT "

# TURBO Cheat
#       if tick < 600:
#            thr = 0.3
        

        self.prev_slip = now.inst.angle
#        print "AI: target_speed=%f, slippage=%f ==> Throttle=%f" % (self.target_speed, diff, thr)

        return thr
