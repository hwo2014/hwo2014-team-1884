import track
import history
import common

class AI(object):
    """ Dummy master class for more intelligent AIs
    """
    def __init__(self, trk, hist, ctrl):
        """ subclasses should use track, and history. 
        """
        self.track = trk
        self.history = hist
        self.control = ctrl
        self.turbo = False
        self.throttle = 0.0

    def run_tick(self, tick, now):
        """ For each game tick, AI's run_tick will be called.
        %now% is an instance of history.RightNow, which contains utility
        functions with precalculated values.
        Override this!
        """
        self.set_throttle(0.2)

    def set_throttle(self, t):
        """ Use this method to control the throttle.
        """
        self.throttle = t

    def legacy_get_throttle(self):
        return self.throttle

    def switch_lane(self, direction):
        """ Use this method to request track switch.
        %direction% must be either "Left" or "Right"
        """
        self.control.switch(direction)

    def turbo_avalable(self):
        self.turbo = True

    def use_turbo(self):
        """ Guess.
#        """
        if self.turbo:
            self.control.turboa_kehiin()
            self.turbo = False
            return True
        else:
            return False
