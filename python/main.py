import json
import socket
import sys
import math
import ai
import krisun_ait
import history
import common
from track import Track

class IceAge(object):

    def __init__(self, socket, trackname, ainame, name, key, cirun):
        self.socket = socket
        self.name = name
        self.key = key
        self.tick = -1
        self.trackname = trackname
        self.penalty = False
        self.ainame = ainame
        self.cirun = cirun
        self.oldspeed = 0
        self.turbo_enabled = []
        self.stash = 0.25

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        if self.cirun:
            return self.msg("join", { "name" : self.name,
                                      "key" : self.key })
        else:
            return self.msg("createRace",
                        { "botId": { "name" : self.name,
                                     "key" : self.key },
# Vaihtoehdot: germany, keimola tai usa
                          "trackName": self.trackname,
                          "password" : "test987",
                          "carCount" : 1,
                          })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switch(self, direction):
        self.msg("switchLane", direction)

    def turboa_kehiin(self):
        self.msg("turbo", "viuuuuu")

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.tick = 0
        self.penalty = False
        self.ping()

    def on_car_positions(self, data):
#        del self.now
        if self.tick == -1:
            self.ping()
            return

        if self.penalty == True:
            self.ping()
            self.tick += 1
            return

        self.history.add_instant(data)
        self.now = history.RightNow(self.track, self.history, self.tick)

# debug... remove if not needed
        if self.tick > 0:
            avgspeed = self.history.distance_covered_total() / self.tick
        else:
            avgspeed = 0.0
        print "Tick: %d," % (self.tick,),
        print "Dist: %fm," % (self.history.distance_covered_this_lap(),),
#        print "Distance (total): %fm," % (self.history.distance_covered_total(),),
        print "Speed: %f (delta: %f)," % (self.history.get_current_speed(), self.history.get_current_speed() - self.oldspeed),
        print "Slip: %f," % (self.now.inst.angle,),
        print "Anglen: %f (deg/m)," % (self.now.piece.get_anglen(self.now.inst.lane),)

# Leave these alone
        newthr = self.ai.run_tick(self.tick, self.now)
        if type(newthr) == "None":
            newthr = self.ai.legacy_get_throttle()
        print "Throttle: %f" % (newthr,)
        self.throttle(newthr)
        if self.history.get_current_speed() - self.oldspeed > 4.0:
            print "BUMB: Sheit! Somebody gave us a kiss!"
        elif self.history.get_current_speed() - self.oldspeed < -4.0:
            print "BUMB: Crap! Safety car ahead of us!"

        self.oldspeed = self.history.get_current_speed()
        self.tick += 1
#        del self.now

    def on_crash(self, data):
        if common.is_us(data["name"]):
            print "! ! ! C R A S H E D ! ! !"
            self.penalty = True
            self.ai.secret += 0.2
        self.ping()

    def on_spawn(self, data):
        if common.is_us(data["name"]):
            print "Back in action"
            self.penalty = False
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.stash = self.ai.secret
        us = common.find_us("car", data["bestLaps"])
        print "Turbo used at: "
        for i in self.turbo_enabled:
            print str(i)
            print "---"
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        print data
        self.track = Track(data)
        self.track.print_track_info()
        self.history = history.History(self.track)

# Replace this by your own AI, that inherits AI class
        if self.ainame == "krisu":
            self.ai = krisun_ait.SwitcherAI(self.track, self.history, self)
        elif self.ainame == "teemu":
            self.ai = None
        else:
            print "Who the fuck is trying to steal our car?"
            sys.exit(0)
        if self.stash < 1.0 and self.stash > 0.25:
            self.ai.secret = self.stash
        else:
            if self.stash < 0.25:
                self.ai.secret = 0.25
            else:
                self.ai.secret = 0.5   # not to over do it
        self.penalty = False
        self.ping()

    def on_turbo(self, data):
        self.ai.turbo_avalable()
        self.turbo_enabled.append(self.now)
#        print data
        
    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'spawn' : self.on_spawn,
            'turboAvailable' : self.on_turbo,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0} : {1}".format(msg_type, data))
                self.ping()
            line = socket_file.readline()

# removing this so I can import from other files...
# probably could import after the main check, but I don't need the main check here
# anyways.
#if __name__ == "__main__":
if len(sys.argv) != 5 and len(sys.argv) != 7:
    print "Usage: ./run host port [track ainame] botname botkey"
    sys.exit(-1)
else:
    if len(sys.argv) == 5:
        host, port, name, key = sys.argv[1:5]
        ainame = "krisu" # laitoin defaultiksi, koska sun ei ollut masterissa
        trackname = "keimola"
        ci_run = True
    else:
        host, port, trackname, ainame, name, key = sys.argv[1:7]
        ci_run = False
#    print("Connecting with parameters:")
#    print("host={0}, port={1}, trackname={2}, name={3}, key={4}".format(*sys.argv[1:6]))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = IceAge(s, trackname, ainame, name, key, ci_run)
    bot.run()
