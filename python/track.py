from __future__ import print_function
import math
import common

class LaneIterator(object):
    """ Simple forward iterator through all lane information.
    Mainly used for using lane offsets in curve calculations due to changed radius.
    
    The iterator returns { "distanceFromCenter" : x, "index" : y } dictionaries
    """
    def __init__(self, master):
        self.idx = 0
        self.main = master
        self.max = master.amount()

    def __iter__(self):
        return self

    def next(self):
        if self.idx < self.max:
            res = self.main.get_lane_info(self.idx)
            self.idx += 1
            return res
        else:
            raise StopIteration("No more lanes")

class Lanes(object):
    """ Holds information about lanes. Only one Lanes object is required per track.
    """
    def __init__(self, lanesInfo):
        self.num = len(lanesInfo)
        self.lanes = lanesInfo

    def iter(self):
        """ Return an iterator to go through all the lanes. 
        """
        return LaneIterator(self)

    def amount(self):
        """ Return number of lanes on this track. """
        return self.num

    def get_lane_info(self, idx):
        """ Return lane information for lane %idx%. 
        None is returned if no lane was found mathing %idx% 
        """
        for lane in self.lanes:
            if lane["index"] == idx:
                return lane
        print("Ooops no lane matched index: %d" % (idx,))
        return None

    def get_lane_offset(self, idx):
        """ Instead of returning lane information in dictionary, just return the
        'distanceFromCenter' value.
        If %idx% lane is not available, return 999!
        """
        t = self.get_lane_info(idx)
        if t != None:
            return t["distanceFromCenter"]
        else:
            return 999


class TrackPiece(object):
    """ Hold all the relevant information about a single piece of track.
    """
    def __init__(self, trackPiece, lanes, index):
        self.index = index
        self.piece = trackPiece
        self.lanes = lanes
        self.lengths = lanes.amount() * [0.0]
        self.anglen = lanes.amount() * [0.0]
        self.calculate_lengths()
            

    def contains_switch(self):
        """ Check if this piece contains a switch.
        """
        return self.piece.has_key("switch")

    def is_straight(self):
        """ Check if this piece is a straight.
        """
        return self.piece.has_key("length")

    def is_curve(self):
        """ Check if this piece is a curve.
        """
        return not self.is_straight()

    def is_curve_left(self):
        return self.piece["angle"] < 0

    def is_curve_right(self):
        return not self.is_curve_left()

    def get_curve_angle(self):
        if self.is_curve():
            return self.piece["angle"]
        else:
            return 0.0

    def calculate_lengths(self):
        """ Used only during initialization (to keep the constructor clean).
        Calculates the length of each lane on this piece.
        """
        if self.is_straight():
            for i in self.lanes.iter():
                self.lengths[i["index"]] = self.piece["length"]
        else:
            for lane in self.lanes.iter():
                laneradius = self.piece["radius"]
                laneoffset = lane["distanceFromCenter"]
                if self.piece["angle"] > 0:
                    laneradius -= laneoffset
                else:
                    laneradius += laneoffset

                self.lengths[lane["index"]] = self.curve_length(laneradius, 
                                                                math.fabs(self.piece["angle"]))
                self.anglen[lane["index"]] = 360/(2*math.pi*laneradius)

    def get_len(self, laneIdx):
        """ Return the length of the current piece for %laneIdx%.
        """
        return self.lengths[laneIdx]

    def get_anglen(self, laneIdx):
        """ Return angular length(?) rad/m for %laneIdx%
        """
        return self.anglen[laneIdx]

    def curve_length(self, radius, angle):
        """ Internal calculations. If you need the length use get_len()
        """
        return float(2*math.pi*radius*angle/360)

    def __str__(self):
        """ Returning A-M-A-Z-I-N-G graphical representation of the piece.
        """
        if self.is_straight():
            if self.contains_switch():
                return "%c[1mS%c[0m" % (0x1B, 0x1B)
            else:
                return "s"
        else:
            s=""
            if self.piece["angle"] < 0:
                if self.contains_switch():
                    s += "%c[1mL" % (0x1B,)
                else:
                    s+= "l"
            else:
                if self.contains_switch():
                    s += "%c[1mR" % (0x1B,)
                else:
                    s += "r"
            absa = math.fabs(self.piece["angle"])
            if absa > 40:
                s += "3"
            elif absa > 20:
                s += "2"
            else:
                s += "1"
            s += "%c[0m" % (0x1B,)
            return s


class Track(object):
    """ Contains information about a track: all its pieces and intricacies.
    Useful variables: 
    pieces = list of TrackPieces
    curves = list of indexes to pieces (TrackPieces)
    switches = list of indexes to pieces (TrackPieces)
    lanes = Lanes object
    """
    def __init__(self, data):        
        self.lanes = Lanes(data["race"]["track"]["lanes"])
        self.pieces = []
        index = 0
        for piece in data["race"]["track"]["pieces"]:
            self.pieces.append(TrackPiece(piece, self.lanes, index))
            index += 1

# Now we have handled all pieces individually
# Track object will aggregate that information
        self.switches = []
        for i in xrange(0,len(self.pieces)):
            if self.pieces[i].contains_switch():
                self.switches.append(i)

        self.curves = []
        for i in xrange(0,len(self.pieces)):
            if self.pieces[i].is_curve():
                self.curves.append(i)

        print("Parsed: %d lanes, %d track pieces, of which %d curves, and %d containing a switch" % (self.lanes.amount(), len(self.pieces), len(self.curves), len(self.switches)))


    def print_track_info(self):
        print("---Track---")
        for piece in self.pieces:
            print("%s-" % (str(piece),), end="")
        print("")

        for i in self.lanes.iter():
            total = 0.0
            print(" --Lane: %d-- " % i["index"])
            for piece in self.pieces:
                plen = piece.get_len(i["index"])
                total += plen
                print("[%f]-" % (plen,), end="")
            print("=[%f]" % (total,))
        print(" ---Curves----- ")
        print("%s" % (self.curves,))
        print("----- Switches -----")
        print("%s" % (self.switches,))
        print("")

    def get_piece_len(self, pindex, lane):
        """ Return length of the piece indexed by %pindex%. The length depends 
        on the chosen %lane%
        """
        return self.pieces[pindex].get_len(lane)

    def get_dist_btw_pieces(self, start, stop, lane):
        """ Calculate distance from the start of %start% piece to the start of
        %stop% piece, using %lane%
        """
        total = 0.0

        if stop < start:      # another lap
            a = self.get_dist_btw_pieces(start, len(self.pieces), lane)
            a += self.get_dist_btw_pieces(0, stop, lane)
            return a

        for i in xrange(start, stop):
            total += self.pieces[i].get_len(lane)
        return total

    def get_dist_n_pieces(self, start, n, lane):
        """ Calculate distance from the start of %start piece until start of %n%
        next pieces, using %lane%.
        """
        if start + n + 1 > len(self.pieces):
            a = self.get_dist_btw_pieces(start, len(self.pieces), lane)
            a += self.get_dist_btw_pieces(0, n - (len(self.pieces) - start) + 1, lane)
            return a
        # else
        return self.get_dist_btw_pieces(start, start+n+1, lane)


    def get_switches(self):
        return self.switches

    def get_curves(self):
        return self.curves

    def get_pieces(self):
        return self.pieces

    def get_lanes(self):
        return self.lanes

    def get_n_pieces(self, bIdx, n, exclusive=True):
        res = []
        if bIdx == len(self.pieces) - 1:
            if exclusive:
                return self.get_n_pieces(0, n, False)
            else:
                return [self.pieces[bIdx]] + self.get_n_pieces(0, n-1, False)

        if bIdx + n >= len(self.pieces):
            still = len(self.pieces)-1-bIdx
            if exclusive:
                return self.get_n_pieces(bIdx, still, True) + self.get_n_pieces(0,n-still+1, False)
            else:
                return self.get_n_pieces(bIdx, still, False) + self.get_n_pieces(0,n-still, False)

        if exclusive:
            bIdx += 1

        for i in xrange(bIdx, bIdx+n):
            res.append(self.pieces[i])
        return res

    def get_next_pieces(self, now, num):
        """ Return a list of %num% next pieces starting piece %now%.
        %now% must be an object of InstantHistory.
        """
        return self.get_n_pieces(now.pi, num, True)

