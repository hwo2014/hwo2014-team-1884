import common

def is_us(name):
	if name.rfind("IceAge") != -1:
		return True
	if name == "Not Mad IC Lab":
		return True
	return False

def find_us(key, data):
	""" Assumes %data% is carPosition message, *AND* that our carname 
	is 'IceAge'
	Returns piecePosition dictionary belonging to our car.
	"""
        for i in data:
		if is_us(i[key]["name"]):
			return i
        print "Help! Can't find myself in carPositions. Looking for id.name=%s" % (weare,)
        print "To fix this, edit common.py, and change the default argument of find_us()"
        return None
 
def extract_pos(carPos):
	""" Assuming %carPos% is carPosition message.
	Returns a list of our current position coordinates. The content on 
	that list is:
	pieceIndex, inPieceDistance, endLaneIndex and lap.
	"""
        we = common.find_us("id", carPos)
        pi = we["piecePosition"]["pieceIndex"]
        ipd = we["piecePosition"]["inPieceDistance"]
        lane = we["piecePosition"]["lane"]["endLaneIndex"]
	lap = we["piecePosition"]["lap"]
	ang = we["angle"]
	return [pi, ipd, lane, lap, ang]

def next_greater_in_list(value, slist):
	""" Return a value that is the next greater than %value% from slist.
	Or if no such value exists, return %value%-1
	"""
        for val in slist:
            if value >= val:
                continue
            return val
	return value-1

