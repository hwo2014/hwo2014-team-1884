import common
import track


class RightNow(object):
    """ This class holds information for the latest moment in time.
    DIfference to the latest InstantHistory is that the IH contains
    the RAW data, where as RightNow will feature all kinds of
    calculations.
    """
    def __init__(self, track, history,  tick):
        self.track = track
        self.history = history
        self.nswitch_p = -1 
        self.ncurve_p = -1
        self.tick = tick
        self.inst = self.history.get_current_instant()
        self.piece = self.track.pieces[self.inst.pi]
        self.ipd_percent = self.inst.ipd / self.piece.get_len(self.inst.lane)

        lanes = self.track.lanes.amount()
        self.nswitch_d = lanes * [0]
        self.ncurve_d = lanes * [0]

        sws = self.track.switches
        crvs = self.track.curves
# First do the pieces, then use a method in Track that calculates distances
        nsp = common.next_greater_in_list(self.inst.pi, sws)
        ncp = common.next_greater_in_list(self.inst.pi, crvs)
#        print "WRAP1: %d, %d" % (ncp, self.inst.pi)

# check for DNFs (in this case means wrap around a lap)
        if nsp == self.inst.pi-1:
            nsp = sws[0]
        if ncp == self.inst.pi-1:
            ncp = crvs[0]
#            print "WRAP2: %d" % (ncp,)
# n{s|c}_p = index to piece of next sw/crv
# self.inst.pi = index to piece we are currently on

# nswitch_p = number of pieces between self.inst.pi and nsp
        if nsp >= self.inst.pi:
            self.nswitch_p = nsp - (self.inst.pi + 1)
        else:
            t = len(self.track.pieces) - (self.inst.pi+1) + nsp
            self.nswitch_p = t

        if ncp >= self.inst.pi:
            self.ncurve_p = ncp - (self.inst.pi + 1)
        else:
# wrap around for lap
            t = len(self.track.pieces) - (self.inst.pi+1) + ncp
#            print "WRAP3: %d, %d, %d, %d" % (t, len(self.track.pieces), self.inst.pi+1, self.ncurve_p)
            self.ncurve_p = t

        for i in xrange(0,lanes):
            d = self.track.get_dist_n_pieces(self.inst.pi, self.nswitch_p, i)
            d -= self.inst.ipd
            self.nswitch_d[i] = d

        for i in xrange(0,lanes):
            d = self.track.get_dist_n_pieces(self.inst.pi, self.ncurve_p, i)
            d -= self.inst.ipd
            self.ncurve_d[i] = d


    def __str__(self):
        a = str("@%d: On a " % (self.tick,),)
        if self.piece.is_straight():
            a += str("straight (%fm) " % (self.piece.get_len(0),),)
        else:
            a += str("curve (%fm, %f deg, %f deg/m) " % (self.piece.get_len(self.inst.lane), self.piece.get_curve_angle(), self.piece.get_anglen(self.inst.lane)),)

        if self.piece.contains_switch():
            a += "with a switch."

        a += str(self.inst)
        return a        

    def dist_to_next_switch(self):
        return self.nswitch_d

    def dist_to_next_curve(self):
        return self.ncurve_d

    def pieces_to_next_switch(self):
        return self.nswitch_p

    def pieces_to_next_curve(self):
        return self.ncurve_p

    def get_location(self):
        return self.inst

    def on_switch(self):
        return self.piece.contains_switch()

    def in_curve(self):
        return self.piece.is_curve()

class HistoryIterator(object):
    """ Returns InstantHistory objects
    """
    def __init__(self, instants, datalist):
        self.data = datalist
        self.insts = instants
        self.cnt = 0
        self.length = len(self.data)
#        print "New iterator: datalist=%s" % (str(self.data),)

    def __iter__(self):
        return self

    def __add__(self, other):
        """ This could be hazardous. Limiting assertion. All iterators must
        not have iterated at all before adding them together!
        """
        assert self.cnt == 0
        assert other.cnt == 0
        return HistoryIterator(self.insts, self.data + other.data)

    def next(self):
        if self.cnt < self.length:
            res = self.insts[self.data[self.cnt]]
            self.cnt += 1
            return res
        else:
            raise StopIteration("No more history")
        

class InstantHistory(object):
    """ Just a struct for now. Later maybe add enemy information as well.
    """
    def __init__(self, carPos):
        coords = common.extract_pos(carPos)
        self.pi = coords[0]
        self.ipd = coords[1]
        self.lane = coords[2]
        self.lap = coords[3]
        self.angle = coords[4]

    def __str__(self):
        return "Lap: %d, Loc: %dp + %fm sliding %f degrees on lane: %d" % (self.lap, self.pi, self.ipd, self.angle, self.lane)

class History(object):
    """ Collect history as carPosition instants.
    """
    def __init__(self, trk):
        self.instants = []
        self.track = trk


    def add_instant(self, carPos):
        """ Add carPos, as the latest instant of history.
        """
        self.instants.append(InstantHistory(carPos))

    def piecewise_one_lap_iterator(self, lap):
        tlist = []
        current = -1
        for i in xrange(0,len(self.instants)):
            inst = self.instants[i]
            if inst.lap != lap:
                continue
            if inst.pi != current:
                tlist.append(i)
                current = inst.pi
        return HistoryIterator(self.instants, tlist)

    def piecewise_all_laps_iterator(self):
        lap = 0
        res = HistoryIterator(self.instants, [])
        while lap < 99:
            it = self.piecewise_one_lap_iterator(lap)
            if it.length == 0:
                break
            res += it
            lap += 1
        return res

    def instantwise_piece_iterator(self, pieceIndex, lap):
        """ Create InstantHistory iterator that iterates over all instants
        that occur on one single piece, on one single lap.
        """
        tlist=[]
        for i in xrange(0,len(self.instants)):
            inst = self.instants[i]
            if inst.lap != lap:
                continue
            if inst.pi != pieceIndex:
                continue
            tlist.append(i)
        return HistoryIterator(self.instants, tlist)

    def distance_covered(self):
        """ Based on historical information, return metres travelled so far.
        BUG: How should this work per lap?
        """

    def distance_covered_total(self):
        """ Return total distance in race.
        """
        now = self.get_current_instant()
        total = 0.0

        # Problem with this iterator is that it adds the current
        # piece as well. However, we haven't finished the current piece.
        for inst in self.piecewise_all_laps_iterator():
            total += self.track.get_piece_len(inst.pi, inst.lane)

        total -= self.track.get_piece_len(now.pi, now.lane)
        return total + now.ipd

    def distance_covered_this_lap(self):
        """ Return distance covered in this lap
        """
        now = self.get_current_instant()
        total = 0.0

        for inst in self.piecewise_one_lap_iterator(now.lap):
            total += self.track.get_piece_len(inst.pi, inst.lane)

        total -= self.track.get_piece_len(now.pi, now.lane)
        return total + now.ipd


    def get_current_instant(self):
        """ Returns current instant in history = current location.
        """
        return self.instants[-1]

    def get_current_piece(self):
        pi = self.instants[-1].pi
        return self.track.pieces[pi]

    def get_current_speed(self):
        """ Exprimental speed calculation.
        """
        if len(self.instants) < 2:
            return 0

        now = self.instants[-1]
        before = self.instants[-2]

        if now.pi == before.pi:
            return now.ipd - before.ipd
        else:
            now_len = self.track.get_piece_len(before.pi, before.lane) + now.ipd
            return now_len - before.ipd


